# TP jenkins

On veut mettre en place l'automatisation sur le projet, avec deux *pipelines* :
- CI :
  - déclenchée à chaque merge request (pas sur les branches `dev` et `main`)
  - applique les étapes suivantes : 
    1. build (front & back)
    2. tests unitaires et d'intégration (front & back)
    3. analyse qualité avec `sonarqube`
    4. construction des images docker
    5. publication des images docker sur un registre `nexus`
  - doit fournir les informations / fichiers suivants :
    - artefacts disponibles dans `jenkins`
    - résultats des tests disponibles dans `jenkins`
    - résultats de l'analyse qualité dans `sonarqube`
    - images docker dans `nexus`
- CD :
  - déclenchée chaque jour à 12h
  - applique les étapes suivantes : 
    1. deploy (database, back & front) sur un environnement de test
    2. tests systèmes et d'acceptation (backend avec postman)
    3. tests systèmes et d'acceptation (frontend avec selenium)
  - doit fournir les informations / fichiers suivants :
    - résultats des tests disponibles dans `jenkins`

Pour chaque étape de chaque pipeline, déterminez :

- quelle commande doit être lancée ?
- quels outils sont nécessaires ? Sous quelle forme seront-ils fournis (image docker, installation sur les runners jenkins, ...) ?
- le cas échéant, comment exploiter les résultats (artifact, rapport de test, ...) ? 

Vous devrez également choisir comment déployer l'application :

- Docker compose ? Kubernetes ? 
- quels sont les ports utilisés par chaque élément de l'application ? Qui doit communiquer avec qui ?
- de quels informations ont-ils besoin ? Sous quelle forme leur transmettre ?