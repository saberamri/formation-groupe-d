import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './pages/admins/admin/admin.component';
import { AdminsComponent } from './pages/admins/admins/admins.component';
import { FormateursComponent } from './pages/formateurs/formateurs.component';
import { GroupesComponent } from './pages/groupes/groupes.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ModulesComponent } from './pages/modules/modules.component';
import { StagiairesComponent } from './pages/stagiaires/stagiaires.component';
import { AdminGuard } from './security/guards/admin.guard';

const routes: Routes = [
  {path: "stagiaires", component: StagiairesComponent, canActivate: [AdminGuard]},
  {path: "formateurs", component: FormateursComponent, canActivate: [AdminGuard]},
  {path: "admins", component: AdminsComponent},
  {path: "admins/:id", component: AdminComponent},
  {path: "groupes", component: GroupesComponent, canActivate: [AdminGuard]},
  {path: "groupes/:id", component: GroupesComponent, canActivate: [AdminGuard]},
  {path: "modules", component: ModulesComponent, canActivate: [AdminGuard]},
  {path: "login", component: LoginComponent},
  {path: "", component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
