import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { debounceTime, filter, map, mergeMap, Observable, startWith } from 'rxjs';
import { Groupe, groupeForm } from 'src/app/models/groupe';
import { Module } from 'src/app/models/module';
import { Participation } from 'src/app/models/participation';
import { Stagiaire } from 'src/app/models/stagiaires';
import { GroupeService } from 'src/app/services/groupe.service';
import { ModuleService } from 'src/app/services/module.service';
import { StagiaireService } from 'src/app/services/stagiaire.service';

@Component({
  selector: 'app-stagiaires',
  templateUrl: './stagiaires.component.html',
  styleUrls: ['./stagiaires.component.css']
})
export class StagiairesComponent implements OnInit {

  @ViewChild("table") table?: MatTable<any>;
  @ViewChild("drawer") drawer?: MatDrawer;
  @ViewChild('groupeInput') groupeInput?: ElementRef<HTMLInputElement>;

  stagiaires: Stagiaire[] = [];
  selectedStagiaire?: Stagiaire;

  sideTitle: string = "";
  stagiaireFormGroupe: FormGroup = Stagiaire.formGroup();

  groupeControl = new FormControl();
  filteredGroupes: Observable<Groupe[]>;

  // moduleControl = new FormControl();
  // filteredModules: Observable<Module[]>;

  constructor(
    public ss: StagiaireService,
    public gs: GroupeService,
    public ms: ModuleService,
    public dialog: MatDialog,
    private router: Router,
  ) {
    this.filteredGroupes = this.groupeControl.valueChanges.pipe(
      startWith(""),
      filter(v => typeof v === "string"),
      debounceTime(300),
      mergeMap(q => gs.search(q, 10)),
      map(lg => lg.filter(g => !(this.groupesFormArray.value as Groupe[]).find(g1 => g.id == g1.id)))
    );
    // this.filteredModules = this.moduleControl.valueChanges.pipe(
    //   startWith(""),
    //   filter(v => typeof v === "string"),
    //   mergeMap(q => ms.search(q, 10)),
    //   map(lm => lm.filter(m => !(this.groupesFormArray.value as Groupe[]).find(g1 => g.id == g1.id)))
    // );
  }

  ngOnInit(): void {
    this.ss.findAll().subscribe(ls => this.stagiaires = ls);
  }

  onAdd() {
    this.selectedStagiaire = { id: 0, prenom: "", nom: "", groupeNames: [], email: "", motDePasse: "", participationsNb: 0, role: "STAGIAIRE"};
    this.sideTitle = "Nouveau stagiaire";
    this.groupesFormArray.clear();
    this.stagiaireFormGroupe.reset();
    this.stagiaireFormGroupe.setValue({ prenom: "", nom: "", groupes: [], email: "", participations: []});
    if (!this.drawer?.opened)
      this.drawer?.open();
  }

  onDelete() {
    if (this.selectedStagiaire) {
      let s = this.selectedStagiaire;
      this.ss.delete(s).subscribe({
        next: () => {
          this.stagiaires.splice(this.stagiaires.indexOf(s), 1);
          this.table?.renderRows();
          this.drawer?.close();
        }
      });
    }
  }

  onSelect(s: Stagiaire) {
    if (this.selectedStagiaire == s && this.drawer?.opened) {
      this.drawer?.close();
    } else {
      this.selectedStagiaire = s;
      this.sideTitle = "Stagiaire " + s.id;
      this.stagiaireFormGroupe.reset();
      this.groupesFormArray.clear();
      this.participationsFormArray.clear();
      this.stagiaireFormGroupe.setValue({ prenom: s.prenom, nom: s.nom, groupes: [], email: s.email, participations: []});
      this.ss.getParticipations(s).subscribe(lp => {
        lp.forEach(p => this.participationsFormArray.push(new FormControl(p)));
      });
      this.ss.getGroupes(s).subscribe(lg => {
        lg.forEach(g => this.groupesFormArray.push(new FormControl(g)));
      });
      if (!this.drawer?.opened)
        this.drawer?.open();
    }
  }

  onCancel() {
    this.selectedStagiaire = undefined;
    this.drawer?.close();
  }

  onSubmit() {
    if (this.stagiaireFormGroupe.invalid) {
      this.stagiaireFormGroupe.markAllAsTouched();
      return;
    }
    if (this.selectedStagiaire) {
      let s = this.selectedStagiaire;
      let lg = this.stagiaireFormGroupe.get("groupes")?.value as Groupe[];
      let lp = this.stagiaireFormGroupe.get("participations")?.value as Participation[];
      Object.assign(s, this.stagiaireFormGroupe.value);
      // g.nom = this.stagiaireFormGroupe.get("nom")?.value;
      // g.nbStagiaires = ls.length;
      if (s.id != 0) {
        this.ss.update(s).pipe(
          mergeMap(() => this.ss.saveGroupes(s, lg).pipe(map(() => {s.groupeNames = lg.map(g => g.nom); return s}))),
          // mergeMap(s => this.ss.saveParticipations(s, lp).pipe(map(() => {s.nbParticipations = lp.length; return s})))
          )
          .subscribe({
            next: (s) => {
              this.stagiaireFormGroupe.markAsPristine();
              this.table?.renderRows();
            }
        });
      } else {
        this.ss.save(s).pipe(
          mergeMap(s => this.ss.saveGroupes(s, lg).pipe(map(() => {s.groupeNames = lg.map(g => g.nom); return s}))),
          // mergeMap(s => this.ss.saveParticipations(s, lp).pipe(map(() => {s.nbParticipations = lp.length; return s})))
          )
          .subscribe({
            next: (s) => {
              this.drawer?.close();
              this.stagiaireFormGroupe.markAsPristine();
              this.stagiaires.push(s);
              this.stagiaires.sort((a,b) => a.id - b.id);
              this.table?.renderRows();
            }
        });
      }
    }
  }

  addGroupe(event: MatAutocompleteSelectedEvent) {
    this.groupesFormArray.push(new FormControl(event.option.value as Groupe));
    if (this.groupeInput)
      this.groupeInput.nativeElement.value = "";
    this.groupeControl.setValue("");
    this.groupeControl.updateValueAndValidity();
    this.groupesFormArray.markAsDirty();
  }

  removeGroupe(index: number) {
    this.groupesFormArray.removeAt(index);
    this.groupeControl.updateValueAndValidity();
    this.groupesFormArray.markAsDirty();
  }

  addModule(event: MatAutocompleteSelectedEvent) {
    // this.groupesFormArray.push(new FormControl(event.option.value as Groupe));
    // if (this.groupeInput)
    //   this.groupeInput.nativeElement.value = "";
    // this.groupeControl.setValue("");
    // this.groupeControl.updateValueAndValidity();
    // this.groupesFormArray.markAsDirty();
  }

  removeModule(index: number) {
    // this.groupesFormArray.removeAt(index);
    // this.groupeControl.updateValueAndValidity();
    // this.groupesFormArray.markAsDirty();
  }

  get groupesFormArray(): FormArray {
    return this.stagiaireFormGroupe.get("groupes") as FormArray;
  }

  get participationsFormArray(): FormArray {
    return this.stagiaireFormGroupe.get("participations") as FormArray;
  }
}
