import { Component, OnInit } from '@angular/core';
import { map, mergeMap } from 'rxjs';
import { Participation } from 'src/app/models/participation';
import { Stagiaire } from 'src/app/models/stagiaires';
import { AuthenticationService } from 'src/app/security/services/authentication.service';
import { ModuleService } from 'src/app/services/module.service';
import { StagiaireService } from 'src/app/services/stagiaire.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  participations: Participation[] = [];

  constructor(
    public ss: StagiaireService,
    public as: AuthenticationService,
  ) { }

  ngOnInit(): void {
    // this.as.connectedUser.pipe(
    //   mergeMap(u => this.ss.getParticipations(u as Stagiaire))
    // ).subscribe(lp => this.participations = lp);
  }

  // onSelect(p: Participation) {

  // }



}
