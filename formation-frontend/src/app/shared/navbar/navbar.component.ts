import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/personne';
import { AuthenticationService } from 'src/app/security/services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: Personne | undefined;

  constructor(
    private as: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.as.connectedUser.subscribe(u => this.user = u);
  }

  onLogout() {
    this.as.logout();
  }

}
