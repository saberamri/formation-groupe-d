import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Groupe } from '../models/groupe';
import { Stagiaire } from '../models/stagiaires';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class GroupeService extends GenericService<Groupe> {

  constructor(hc: HttpClient) {
    super(hc, environment.backendUrl + "/groupes");
  }

   search(q: string, nb: number = 0): Observable<Groupe[]> {
    return this.hc.get<Groupe[]>(this.url + "/search?q=" + q + (nb ? ("&page=0&size="+nb) : ""));
  }

   getStagiaires(g: Groupe): Observable<Stagiaire[]> {
     return this.hc.get<Stagiaire[]>(this.url + "/" + g.id + "/stagiaires");
   }

   saveStagiaires(g: Groupe, ss: Stagiaire[]): Observable<any> {
     return this.hc.post(this.url + "/" + g.id + "/stagiaires", ss);
   }



}
