package fr.formation.categories.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formation.categories.dto.ModuleDto;
import fr.formation.categories.mappers.ModuleMapper;
import fr.formation.categories.models.Module;
import fr.formation.categories.repositories.ModuleRepository;

@Service
public class ModuleService extends GenericService<ModuleMapper, ModuleRepository, Module, ModuleDto> {

	@Autowired
	private ParticipationService ps;
	
	@Override
	public ModuleDto save(ModuleDto gd) {
		// TODO Auto-generated method stub
		return super.save(gd);
	}

//	@Override
//	@Transactional
//	public ModuleDto update(ModuleDto gd) {
//		gd.getParticipations().forEach(p -> {
//			p.setModule(gd);
//			if (p.getId() == 0)
//				ps.save(p);
//			else
//				ps.update(p);
//		});
//		return super.update(gd);
//	}
	
	
	
}
