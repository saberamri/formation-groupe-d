package fr.formation.categories.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.formation.categories.models.Groupe;

@Repository
public interface GroupeRepository extends JpaRepository<Groupe, Integer> {

	@Query("from Groupe g where g.nom like %:q%")
	public Collection<Groupe> search(String q);

	@Query("from Groupe g where g.nom like %:q%")
	public List<Groupe> search(String q, Pageable page);

}
