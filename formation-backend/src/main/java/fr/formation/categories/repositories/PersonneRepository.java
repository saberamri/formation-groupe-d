package fr.formation.categories.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.categories.models.Personne;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Integer> {

	public Optional<Personne> findByEmail(String email);
	
}
