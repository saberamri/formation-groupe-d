package fr.formation.categories.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.categories.models.Participation;

@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Integer> {

}
