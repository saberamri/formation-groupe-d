package fr.formation.categories.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Stagiaire extends Personne {
	
	private String code;

	@ManyToMany(mappedBy = "stagiaires")
	@Builder.Default
	private Set<Groupe> groupes = new HashSet<>();
	
	@OneToMany(mappedBy = "stagiaire")
	@Builder.Default
	private Set<Participation> participations = new HashSet<>();
}
