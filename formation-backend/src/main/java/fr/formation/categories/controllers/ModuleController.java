package fr.formation.categories.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.categories.dto.ModuleDto;
import fr.formation.categories.services.ModuleService;

@RestController
@RequestMapping("/modules")
public class ModuleController extends GenericController<ModuleDto, ModuleService> {

}
