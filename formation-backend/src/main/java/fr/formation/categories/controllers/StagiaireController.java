package fr.formation.categories.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.dto.ParticipationDto;
import fr.formation.categories.dto.StagiaireDto;
import fr.formation.categories.services.StagiaireService;

@RestController
@RequestMapping("/stagiaires")
public class StagiaireController extends GenericController<StagiaireDto, StagiaireService> {


	@GetMapping("search")
	public Collection<StagiaireDto> search(@RequestParam() String q, Pageable pageable) {
		return service.search(q, pageable);
	}
	
	@GetMapping("{id}/groupes")
	public Collection<GroupeDto> getGroupes(@PathVariable int id) {
		return service.getGroupes(id);
	}

	@PostMapping("{id}/groupes")
	public void setGroupes(@PathVariable int id, @RequestBody List<GroupeDto> groupes) {
		service.setGroupes(id, groupes);
	}
	
	@GetMapping("{id}/participations")
	public Collection<ParticipationDto> getParticipations(@PathVariable int id) {
		return service.getParticipations(id);
	}
	
	
}
