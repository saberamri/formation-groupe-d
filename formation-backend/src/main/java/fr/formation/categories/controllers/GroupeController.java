package fr.formation.categories.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.dto.StagiaireDto;
import fr.formation.categories.services.GroupeService;

@RestController
@RequestMapping("/groupes")
public class GroupeController extends GenericController<GroupeDto, GroupeService> {

	@GetMapping("search")
	public Collection<GroupeDto> search(@RequestParam() String q, Pageable pageable) {
		return service.search(q, pageable);
	}
	
	@GetMapping("{id}/stagiaires")
	public Collection<StagiaireDto> getStagiaires(@PathVariable int id) {
		return service.getStagiaires(id);
	}

	@PostMapping("{id}/stagiaires")
	public void setStagiaires(@PathVariable int id, @RequestBody List<StagiaireDto> stagiaires) {
		service.setStagiaires(id, stagiaires);
	}
	
	@PostMapping("{idGroupe}/stagiaires/{idStagiaire}")
	public void addStagiaire(@PathVariable int idGroupe, @PathVariable int idStagiaire) {
		service.addStagiaire(idStagiaire, idGroupe);
	}
	
}
