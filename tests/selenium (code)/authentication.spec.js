const { Builder, By, NoSuchElementError } = require('selenium-webdriver')
const assert = require('assert')


describe('authentication', function() {

    this.timeout(300000)

    let driver

    beforeEach(async function() {
        // create driver
        driver = await new Builder()
            .usingServer('http://51.91.135.166:4444/wd/hub')
            .forBrowser('firefox')
            .build()
    })

    afterEach(async function() {
        // quit driver
        await driver.quit()
    })


    let data = [
        {
            testCaseDescription: "[FOR-61] Authenticating as admin succeed",
            username: "a",
            password: "a",
            assert: async function() {
                // verify that we are on the home page
                assert((await driver.getCurrentUrl()) === 'http://51.91.134.198/');
                // verify the existence of the logout button
                assert((await driver.findElement(By.css('button mat-icon')).getText()).toLowerCase().includes('logout'));

            } 
        },
        {
            testCaseDescription: "[FOR-61] Authenticating as formateur succeed",
            username: "f",
            password: "f",
            assert: async function() {
                // verify that we are on the home page
                assert((await driver.getCurrentUrl()) === 'http://51.91.134.198/');
                // verify the existence of the logout button
                assert((await driver.findElement(By.css('button mat-icon')).getText()).toLowerCase().includes('logout'));

            } 
        },
        {
            testCaseDescription: "[FOR-61] Authenticating as stagiaire succeed",
            username: "s",
            password: "s",
            assert: async function() {
                // verify that we are on the home page
                assert((await driver.getCurrentUrl()) === 'http://51.91.134.198/');
                // verify the existence of the logout button
                assert((await driver.findElement(By.css('button mat-icon')).getText()).toLowerCase().includes('logout'));

            } 
        },
        {
            testCaseDescription: "[FOR-61] Authenticating with an invalid password fails",
            username: "a",
            password: "invalid",
            assert: async function() {
                // verify that we are on the home page
                assert((await driver.getCurrentUrl()) === 'http://51.91.134.198/login');
                // verify the existence of the logout button
                assert((await driver.findElement(By.css('form p.error')).getText()).toLowerCase().includes('invalid'));

            } 
        },
        {
            testCaseDescription: "[FOR-61] Authenticating with an invalid username fails",
            username: "invalid",
            password: "a",
            assert: async function() {
                // verify that we are on the home page
                assert((await driver.getCurrentUrl()) === 'http://51.91.134.198/login');
                // verify the existence of the logout button
                assert((await driver.findElement(By.css('form p.error')).getText()).toLowerCase().includes('invalid'));

            } 
        },
    ]


    async function login(username, password) {
        // Open page
        await driver.get('http://51.91.134.198/login')
        // Type username student into Username field
        await driver.findElement(By.css('input[name=username]')).sendKeys(username)
        // Type password Password123 into Password field
        await driver.findElement(By.css('input[name=password]')).sendKeys(password)
        // Push Submit button
        await driver.findElement(By.css('form > button')).click()
    }


    data.forEach(vars => {
        it(vars.testCaseDescription, async function (){
            await login(vars.username, vars.password);
            await vars.assert();
        });
    });


})